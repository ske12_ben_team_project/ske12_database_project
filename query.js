var mysql = require('mysql');

var connection;

exports.createConnection = function() {
	connection = mysql.createConnection( {
		host : 'localhost',
		user : 'root',
		password : '',
		database : 'bangkok2nite'
	});

	connection.connect(function(err) {
		if(err) {
			console.error('Error connecting: ' + err.stack);
    		return;
		}

		console.log('Connected as ID ' + connection.threadId);
	});
}

exports.getLastestEvents = function(callback) {
	exports.createConnection();
	connection.query('SELECT event_name, location.location_name, event_description, DATE_FORMAT(start_date, "%d/%m/%Y") AS begin_date, DATE_FORMAT(end_date, "%d/%m/%Y") AS end_date, start_time, end_time, price, event_picture.URL FROM event JOIN location ON event.location_id = location.location_id JOIN event_picture ON event.event_id = event_picture.event_id WHERE start_date < CAST(CURRENT_TIMESTAMP AS DATE) GROUP BY event.event_id ORDER BY start_date DESC LIMIT 6',
		function(err, rows, fields) {
		if(err) throw err;
		callback(rows);
	});

	connection.end();
}

exports.getLocationsByPage = function(page, callback) {
	exports.createConnection();
	connection.query('SELECT location.location_id, location_name, category.category_name, location_picture.URL, address, location_description, contact, social.facebook FROM location JOIN category ON location.category_id = category.category_id JOIN social ON location.social_id = social.social_id JOIN location_picture ON location.location_id = location_picture.location_id GROUP BY location.location_id LIMIT 6 OFFSET ' + (page-1)*6, 
		function(err, rows, fields) {
			if(err) throw err;
			callback(rows);
	});

	connection.end();
}

exports.getPicturesFromLocation = function(id, callback) {
	exports.createConnection();
	connection.query('SELECT URL FROM location_picture WHERE location_picture.location_id = ' + id, 
		function(err, rows, fields) {
			if(err) throw err;
			callback(rows);
	});

	connection.end();	
}

exports.getEventsByPage = function(page, callback) {
	exports.createConnection();
	connection.query('SELECT event_name, location.location_name, event_description, DATE_FORMAT(start_date, "%d/%m/%Y") AS begin_date, DATE_FORMAT(end_date, "%d/%m/%Y") AS end_date, start_time, end_time, price, event_picture.URL FROM event JOIN location ON event.location_id = location.location_id JOIN event_picture ON event.event_id = event_picture.event_id GROUP BY event.event_id LIMIT 6 OFFSET ' + (page-1)*6, 
		function(err, rows, fields) {
			if(err) throw err;
			callback(rows);
	});

	connection.end();
}

exports.getLocationsByCategoryAndPage = function(category, page, callback) {
	exports.createConnection();
	connection.query('SELECT location.location_id, location_name, category.category_name, location_picture.URL, address, location_description, contact, social.facebook FROM location JOIN category ON location.category_id = category.category_id JOIN social ON location.social_id = social.social_id JOIN location_picture ON location.location_id = location_picture.location_id WHERE category.category_name = ' + '"' + category + '"' + ' GROUP BY location.location_id LIMIT 6 OFFSET ' + (page-1)*6,
		function(err, rows, fields) {
			if(err) throw err;
			callback(rows);
	});

	connection.end();
}

exports.getNumberOfLocationByCategory = function(category, callback) {
	exports.createConnection();
	connection.query('SELECT COUNT(location.location_id) AS numbers FROM location JOIN category ON location.category_id = category.category_id WHERE category.category_name = ' + '"' + category + '"', 
		function(err, rows, fields) {
			if(err) throw err;
			callback(rows);
		});

	connection.end();
}

exports.getCategoryDescription = function(callback) {
	exports.createConnection();
	connection.query('SELECT category_description FROM category', 
		function(err, rows, fields) {
			if(err) throw err;
			callback(rows);
	});

	connection.end();
}