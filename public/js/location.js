var initComponents = function(num) {
  $.getJSON('http://localhost:6969/locations/page/' + num, function(result) {
    informComponents(result);
  });
};

var initComponentsFiltered = function(category, num) {
  $.getJSON('http://localhost:6969/locations/' + category + '/page/' + num, function(result) {
    informComponents(result);
  });
};

var informComponents = function(result) {
  for(var i = 0; i < 6; i++) {
      $('#location-' + (i+1)).addClass('hidden');
    }

    for(var i = 0; i < result.length; i++) {
      $('#location-' + (i+1)).removeClass('hidden');
      $('#location-' + (i+1) + '-info h4').text(result[i].location_name);
      $('#location-' + (i+1) + '-info p').text(result[i].category_name);
      $('#location-' + (i+1) + '-img img').attr('src', result[i].URL);

      $('#location-' + (i+1) + '-img').bind('click', {currentIndex : i}, function(event) {
        $('#locationModal-body h2').text(result[event.data.currentIndex].location_name);
        $('#locationModal-body p').text(result[event.data.currentIndex].location_description);
        $('#locationModal-body img').attr('src', result[event.data.currentIndex].URL);
        $('.item-intro.text-muted').text(result[event.data.currentIndex].category_name);

        $('#li-address').text("Address: " + result[event.data.currentIndex].address);           

        $('#li-contact').text("Contact: " + result[event.data.currentIndex].contact);

        $('#li-facebook').text(result[event.data.currentIndex].facebook);

        $('#li-facebook a').attr('href', result[event.data.currentIndex].facebook);

        var temp = result[event.data.currentIndex].location_id;

        var items = $('#locationModal-galleries ul li');

        $.getJSON('http://localhost:6969/locations/galleries/' + temp, function(result) {
          for(var j = 0; j < items.length; j++) {
            $($(items[j]).children()).attr('href', result[j].URL);
            $($(items[j]).children().children()).attr('src', result[j].URL);
          }
        });
      });
    }
};

var isNotFiltered = function(category) {
  return category !== 'Nightclubs' && category !== 'Pubs' && category !== 'Bars' &&  category !== 'Night Markets';
};

var initPagination = function(category, num) {

  $('#page-selection').bootpag ({
    total: num,
    maxVisible: num,
    leaps: true,
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
  }).unbind( "page" ).on("page", function(event, num) {
    if(isNotFiltered(category)) {
     initComponents(num); 
   }

   else initComponentsFiltered(category, num);
 });
}

var initPictureGallery = function() {
  $(".fancybox-thumb").fancybox({
    prevEffect  : 'none',
    nextEffect  : 'none',
    helpers : {
      title : {
        type: 'outside'
      },
      thumbs  : {
        width : 50,
        height  : 50
      }
    }
  });
}

var getNumberOfLocationByCategory = function(category) {
  var num = 0;

  if(isNotFiltered(category)) {
    num = 5;
    initPagination(category, num);
  }

  else {
    $.getJSON('/locations/' + category +'/num', function(result) {
      num = Math.ceil(parseInt(result[0].numbers) / 6);
      initPagination(category, num);
    });
  }
};

$(document).ready(function() {
  initPictureGallery();
  initComponents(1);
  initPagination('All', 5);
});

$('.dropup ul li').click(function() {
    var category = $(this).children().text();
    var currentPage = $('#page-selection').find('li.active').children().text();
    $('#dropup-button').text(category);

    getNumberOfLocationByCategory(category);

    if(isNotFiltered(category)) {
      initComponents(currentPage);
    }

    else initComponentsFiltered(category, currentPage);

  }); 