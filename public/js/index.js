var initComponents = function() {
  $.getJSON('http://localhost:6969/preview', function(result) {

    for(var i = 0; i < 6; i++) {
      $('#event-' + (i+1)).addClass('hidden');
    }

    for(var i = 0; i < result.length; i++) {
      $('#event-' + (i+1)).removeClass('hidden');
      $('#event-' + (i+1) + '-info h4').text(result[i].event_name);
      $('#event-' + (i+1) + '-info p').text(result[i].location_name);
      $('#event-' + (i+1) + '-img img').attr('src', result[i].URL);

        $('#event-' + (i+1) + '-img').bind('click', {currentIndex : i}, function(event) {
        $('#eventModal-body h2').text(result[event.data.currentIndex].event_name);
        $('#eventModal-body p').text(result[event.data.currentIndex].event_description);
        $('#eventModal-body img').attr('src', result[event.data.currentIndex].URL);
        $('.item-intro.text-muted').text(result[event.data.currentIndex].location_name);

        if(result[event.data.currentIndex].begin_date === result[event.data.currentIndex].end_date) {
          $('#li-date').text("Date: " + result[event.data.currentIndex].begin_date);
        }

        else {
          $('#li-date').text("Date: " + result[event.data.currentIndex].begin_date + " - " + result[event.data.currentIndex].end_date);
        }

        $('#li-time').text("Time: " + result[event.data.currentIndex].start_time.substring(0, 5) + " - " + result[event.data.currentIndex].end_time.substring(0, 5));

        $('#li-price').text("Price: " + result[event.data.currentIndex].price + " Baht");
      });
    }
  });
};