var express = require('express');

var app = express();

var bodyParser = require('body-parser');

var path = require('path');

var port = process.env.PORT || 6969;

var query = require('./query.js');

app.use(bodyParser.json());

app.use(bodyParser.urlencoded( {
	extended : true
}));

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'jade');

app.use('/public', express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
	res.render('index.jade');
});

app.get('/events', function(req, res) {
	res.render('event.jade');
});

app.get('/locations', function(req, res) {
	res.render('location.jade');
});

app.get('/about', function(req, res) {
	res.render('about.jade');
});

app.get('/locations/page/:page', function(req, res) {
	var page = req.params.page;

	var callback = function(data) {
		res.send(data);
	}

	query.getLocationsByPage(page, callback);
});

app.get('/locations/galleries/:id', function(req, res){
	var id = req.params.id;

	var callback = function(data) {
		res.send(data);
	}

	query.getPicturesFromLocation(id, callback);
});

app.get('/events/page/:page', function(req, res) {
	var page = req.params.page;

	var callback = function(data) {
		res.send(data);
	}

	query.getEventsByPage(page, callback);
});

app.get('/locations/:category/page/:page', function(req, res) {
	var category = req.params.category;
	var page = req.params.page;

	var callback = function(data) {
		res.send(data);
	}

	query.getLocationsByCategoryAndPage(category, page, callback);
});

app.get('/locations/:category/num', function(req, res) {
	var category = req.params.category;

	var callback = function(data) {
		res.send(data);
	}

	query.getNumberOfLocationByCategory(category, callback);
});

app.get('/about/categories', function(req, res) {
	var callback = function(data) {
		res.send(data);
	}

	query.getCategoryDescription(callback);
});

app.get('/preview', function(req, res) {
	var callback = function(data) {
		res.send(data);
	}

	query.getLastestEvents(callback);
});

app.listen(port, function() {
	console.log('Starting node.js on port ' + port);

});